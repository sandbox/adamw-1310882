LOCK TABLES `custom_breadcrumb` WRITE;
/*!40000 ALTER TABLE `custom_breadcrumb` DISABLE KEYS */;
INSERT INTO `custom_breadcrumb` VALUES (1,'find\r\n[title-raw]','[field_unit_top-path]\r\n[field_unit_top-path]/[field_code-raw]','return !empty($node->field_unit_top[0][\'nid\']);','unit'),(2,'[field_parent_unit-title]\r\n[title-raw]','[field_parent_unit-path]\r\n[field_parent_unit-path]/[field_code-raw]','return empty($node->field_unit_top[0][\'nid\']);','unit');
/*!40000 ALTER TABLE `custom_breadcrumb` ENABLE KEYS */;
UNLOCK TABLES;
