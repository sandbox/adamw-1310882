INSERT INTO `rules_rules` VALUES ('rules_7','a:9:{s:5:\"#type\";s:4:\"rule\";s:4:\"#set\";s:30:\"event_revisioning_post_publish\";s:6:\"#label\";s:39:\"Allow new revisions for published nodes\";s:7:\"#active\";i:1;s:7:\"#weight\";s:2:\"-5\";s:11:\"#categories\";a:0:{}s:7:\"#status\";s:6:\"custom\";s:11:\"#conditions\";a:0:{}s:8:\"#actions\";a:1:{i:0;a:5:{s:5:\"#type\";s:6:\"action\";s:9:\"#settings\";a:5:{s:10:\"target_sid\";s:1:\"2\";s:10:\"state_name\";s:15:\"New/In Progress\";s:5:\"force\";i:0;s:16:\"workflow_comment\";s:28:\"Action set %title to %state.\";s:13:\"#argument map\";a:1:{s:4:\"node\";s:4:\"node\";}}s:5:\"#name\";s:45:\"rules_core_workflow_select_given_state_action\";s:5:\"#info\";a:6:{s:6:\"module\";s:8:\"Workflow\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:5:\"label\";s:7:\"Content\";s:4:\"type\";s:4:\"node\";}}s:5:\"label\";s:45:\"Change workflow state of content to new state\";s:4:\"base\";s:25:\"rules_core_action_execute\";s:11:\"action_name\";s:34:\"workflow_select_given_state_action\";s:12:\"configurable\";b:1;}s:7:\"#weight\";d:0;}}}'),('rules_8','a:9:{s:5:\"#type\";s:4:\"rule\";s:4:\"#set\";s:28:\"event_workflow_state_changed\";s:6:\"#label\";s:19:\"Workflow: Unpublish\";s:7:\"#active\";i:1;s:7:\"#weight\";s:1:\"0\";s:11:\"#categories\";a:0:{}s:7:\"#status\";s:6:\"custom\";s:11:\"#conditions\";a:1:{i:0;a:5:{s:5:\"#type\";s:9:\"condition\";s:9:\"#settings\";a:3:{s:10:\"from_state\";a:1:{s:3:\"ANY\";s:3:\"ANY\";}s:8:\"to_state\";a:1:{i:6;s:1:\"6\";}s:13:\"#argument map\";a:2:{s:9:\"old_state\";s:9:\"old_state\";s:9:\"new_state\";s:9:\"new_state\";}}s:5:\"#name\";s:25:\"workflow_check_transition\";s:5:\"#info\";a:3:{s:5:\"label\";s:55:\"Check workflow transition from Any state to Unpublished\";s:9:\"arguments\";a:2:{s:9:\"old_state\";a:2:{s:4:\"type\";s:14:\"workflow_state\";s:5:\"label\";s:18:\"Old workflow state\";}s:9:\"new_state\";a:2:{s:4:\"type\";s:14:\"workflow_state\";s:5:\"label\";s:18:\"New workflow state\";}}s:6:\"module\";s:8:\"Workflow\";}s:7:\"#weight\";d:0;}}s:8:\"#actions\";a:1:{i:0;a:5:{s:7:\"#weight\";d:0;s:5:\"#info\";a:8:{s:5:\"label\";s:25:\"Unpublish Updated content\";s:6:\"module\";s:4:\"Node\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:5:\"label\";s:7:\"Content\";s:4:\"type\";s:4:\"node\";}}s:4:\"base\";s:25:\"rules_core_action_execute\";s:11:\"action_name\";s:21:\"node_unpublish_action\";s:12:\"configurable\";b:0;s:14:\"label callback\";s:30:\"rules_core_node_label_callback\";s:14:\"label_skeleton\";s:15:\"Unpublish @node\";}s:5:\"#name\";s:32:\"rules_core_node_unpublish_action\";s:9:\"#settings\";a:2:{s:9:\"auto_save\";i:1;s:13:\"#argument map\";a:1:{s:4:\"node\";s:4:\"node\";}}s:5:\"#type\";s:6:\"action\";}}}'),('rules_6','a:9:{s:5:\"#type\";s:4:\"rule\";s:4:\"#set\";s:28:\"event_workflow_state_changed\";s:6:\"#label\";s:17:\"Workflow: Publish\";s:7:\"#active\";i:1;s:7:\"#weight\";s:3:\"-10\";s:11:\"#categories\";a:0:{}s:7:\"#status\";s:6:\"custom\";s:11:\"#conditions\";a:1:{i:0;a:5:{s:7:\"#weight\";d:0;s:5:\"#info\";a:3:{s:5:\"label\";s:53:\"Check workflow transition from Any state to Published\";s:9:\"arguments\";a:2:{s:9:\"old_state\";a:2:{s:4:\"type\";s:14:\"workflow_state\";s:5:\"label\";s:18:\"Old workflow state\";}s:9:\"new_state\";a:2:{s:4:\"type\";s:14:\"workflow_state\";s:5:\"label\";s:18:\"New workflow state\";}}s:6:\"module\";s:8:\"Workflow\";}s:5:\"#name\";s:25:\"workflow_check_transition\";s:9:\"#settings\";a:3:{s:10:\"from_state\";a:1:{s:3:\"ANY\";s:3:\"ANY\";}s:8:\"to_state\";a:1:{i:5;s:1:\"5\";}s:13:\"#argument map\";a:2:{s:9:\"old_state\";s:9:\"old_state\";s:9:\"new_state\";s:9:\"new_state\";}}s:5:\"#type\";s:9:\"condition\";}}s:8:\"#actions\";a:1:{i:0;a:5:{s:5:\"#type\";s:6:\"action\";s:9:\"#settings\";a:1:{s:13:\"#argument map\";a:1:{s:4:\"node\";s:4:\"node\";}}s:5:\"#name\";s:39:\"revisioning_rules_action_publish_latest\";s:5:\"#info\";a:3:{s:6:\"module\";s:11:\"Revisioning\";s:5:\"label\";s:40:\"Publish the most recent pending revision\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:4:\"type\";s:4:\"node\";s:5:\"label\";s:7:\"content\";}}}s:7:\"#weight\";d:0;}}}'),('rules_9','a:9:{s:5:\"#type\";s:4:\"rule\";s:4:\"#set\";s:32:\"event_revisioning_post_unpublish\";s:6:\"#label\";s:41:\"Allow new revisions for unpublished nodes\";s:7:\"#active\";i:1;s:7:\"#weight\";s:1:\"5\";s:11:\"#categories\";a:0:{}s:7:\"#status\";s:6:\"custom\";s:11:\"#conditions\";a:0:{}s:8:\"#actions\";a:1:{i:0;a:5:{s:7:\"#weight\";d:0;s:5:\"#info\";a:6:{s:6:\"module\";s:8:\"Workflow\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:5:\"label\";s:7:\"Content\";s:4:\"type\";s:4:\"node\";}}s:5:\"label\";s:45:\"Change workflow state of content to new state\";s:4:\"base\";s:25:\"rules_core_action_execute\";s:11:\"action_name\";s:34:\"workflow_select_given_state_action\";s:12:\"configurable\";b:1;}s:5:\"#name\";s:45:\"rules_core_workflow_select_given_state_action\";s:9:\"#settings\";a:5:{s:10:\"target_sid\";s:1:\"2\";s:10:\"state_name\";s:15:\"New/In Progress\";s:5:\"force\";i:1;s:16:\"workflow_comment\";s:28:\"Action set %title to %state.\";s:13:\"#argument map\";a:1:{s:4:\"node\";s:4:\"node\";}}s:5:\"#type\";s:6:\"action\";}}}'),('rules_11','a:9:{s:5:\"#type\";s:4:\"rule\";s:4:\"#set\";s:29:\"event_revisioning_post_revert\";s:6:\"#label\";s:15:\"Revert Revision\";s:7:\"#active\";i:1;s:7:\"#weight\";s:1:\"0\";s:11:\"#categories\";a:0:{}s:7:\"#status\";s:6:\"custom\";s:11:\"#conditions\";a:0:{}s:8:\"#actions\";a:3:{i:1;a:5:{s:7:\"#weight\";d:1;s:5:\"#info\";a:7:{s:5:\"label\";s:26:\"Unpublish current revision\";s:14:\"label callback\";b:0;s:6:\"module\";s:8:\"Workflow\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:5:\"label\";s:7:\"Content\";s:4:\"type\";s:4:\"node\";}}s:4:\"base\";s:25:\"rules_core_action_execute\";s:11:\"action_name\";s:34:\"workflow_select_given_state_action\";s:12:\"configurable\";b:1;}s:5:\"#name\";s:45:\"rules_core_workflow_select_given_state_action\";s:9:\"#settings\";a:5:{s:10:\"target_sid\";s:1:\"6\";s:10:\"state_name\";s:11:\"Unpublished\";s:5:\"force\";i:0;s:16:\"workflow_comment\";s:28:\"Action set %title to %state.\";s:13:\"#argument map\";a:1:{s:4:\"node\";s:16:\"current_revision\";}}s:5:\"#type\";s:6:\"action\";}i:0;a:5:{s:7:\"#weight\";d:2;s:5:\"#info\";a:3:{s:6:\"module\";s:11:\"Revisioning\";s:5:\"label\";s:40:\"Publish the most recent pending revision\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:4:\"type\";s:4:\"node\";s:5:\"label\";s:7:\"content\";}}}s:5:\"#name\";s:39:\"revisioning_rules_action_publish_latest\";s:9:\"#settings\";a:1:{s:13:\"#argument map\";a:1:{s:4:\"node\";s:4:\"node\";}}s:5:\"#type\";s:6:\"action\";}i:2;a:5:{s:5:\"#type\";s:6:\"action\";s:9:\"#settings\";a:5:{s:10:\"target_sid\";s:1:\"2\";s:10:\"state_name\";s:15:\"New/In Progress\";s:5:\"force\";i:0;s:16:\"workflow_comment\";s:28:\"Action set %title to %state.\";s:13:\"#argument map\";a:1:{s:4:\"node\";s:4:\"node\";}}s:5:\"#name\";s:45:\"rules_core_workflow_select_given_state_action\";s:5:\"#info\";a:7:{s:5:\"label\";s:39:\"Allow new revisions for published nodes\";s:14:\"label callback\";b:0;s:6:\"module\";s:8:\"Workflow\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:5:\"label\";s:7:\"Content\";s:4:\"type\";s:4:\"node\";}}s:4:\"base\";s:25:\"rules_core_action_execute\";s:11:\"action_name\";s:34:\"workflow_select_given_state_action\";s:12:\"configurable\";b:1;}s:7:\"#weight\";d:3;}}}');
/*!40000 ALTER TABLE `rules_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rules_scheduler`
--

DROP TABLE IF EXISTS `rules_scheduler`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `rules_scheduler` (
  `tid` int(10) unsigned NOT NULL auto_increment,
  `set_name` varchar(255) NOT NULL default '',
  `date` datetime NOT NULL,
  `arguments` text,
  `identifier` varchar(255) default '',
  PRIMARY KEY  (`tid`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `rules_scheduler`
--

LOCK TABLES `rules_scheduler` WRITE;
/*!40000 ALTER TABLE `rules_scheduler` DISABLE KEYS */;
/*!40000 ALTER TABLE `rules_scheduler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rules_sets`
--

DROP TABLE IF EXISTS `rules_sets`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `rules_sets` (
  `name` varchar(255) NOT NULL default '',
  `data` longblob,
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `rules_sets`
--

LOCK TABLES `rules_sets` WRITE;
/*!40000 ALTER TABLE `rules_sets` DISABLE KEYS */;
INSERT INTO `rules_sets` VALUES ('rules_set_1','a:4:{s:5:\"label\";s:44:\"Example: Empty rule set working with content\";s:9:\"arguments\";a:1:{s:4:\"node\";a:2:{s:4:\"type\";s:4:\"node\";s:5:\"label\";s:7:\"Content\";}}s:10:\"categories\";a:1:{i:0;s:7:\"example\";}s:6:\"status\";s:6:\"custom\";}');
/*!40000 ALTER TABLE `rules_sets` ENABLE KEYS */;
UNLOCK TABLES;
